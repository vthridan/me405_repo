'''@file Touch_Driver.py
   @brief This script holds the code to run the resitive touch panel
   @details 
   @author Nicholas Holman
   @author Vikram Thridandum
'''

## Import necessary classes 
import pyb
import utime
import array

class Touch_Driver:
    '''@brief   A driver that runs an ER-TFT080-1 Resistive Touch Panel
       @details 
    '''
    
    def __init__(self,xmpin, ympin,xppin,yppin):
        '''@brief Initializes the touchdriver class pins and preallocates buffer arrays for the first three methods.
           @param xmpin The pin to which the xm pin of the touchscreen is attached.
           @param ympin The pin to which the ym pin of the touchscreen is attached.
           @param xppin The pin to which the xp pin of the touchscreen is attached.
           @param yppin The pin to which the yp pin of the touchscreen is attached
        '''
        
        ## Create an array for the position coordinates
        self.pos = array.array('f',[0, 0, 0])
        ## Boolean object that signals when the screen is being touched
        self.tch = False
        
        ## Array witht the dimensions of the touch screen
        self.Cal = array.array('h',[300, 3820, 480, 3650])
        self.yCal = 107/(self.Cal[3]-self.Cal[2])
        self.xCal = 182/(self.Cal[1]-self.Cal[0])
        self.cCal = [2060,2065]
        
        ## X positional buffer for reading three x positions from the sensor
        self.xbuf = array.array('h',[0, 0, 0])
        ## Y positional buffer for reading three y positions from the sensor
        self.ybuf = array.array('h',[0, 0, 0])
        ## Z positional buffer for reading three z readings from the sensor
        self.zbuf = array.array('h',[0, 0, 0])
        
        ## Pin object of the xm RTP Pin
        self.xm = pyb.Pin(xmpin)       
        ## Pin object of the yp RTP Pin
        self.yp = pyb.Pin(yppin)     
        ## Pin object of the xp RTP Pin
        self.xp = pyb.Pin(xppin)     
        ## Pin object of the ym RTP Pin
        self.ym = pyb.Pin(ympin)
        
        #Preset pins to dset 4
        self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
        self.phigh.high()
        self.plow = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
        self.plow.low()
        
        self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
        self.pread = pyb.ADC(self.xm)
        self.dset = 4
        
        
    def xreader(self):
        '''@brief  read the x position from the resistive touch panel
           @details 
           @return self.pos  the x value of the touch location in [mm]
        '''
        
        # Reset pins for x-read in optimal configuration 
        if self.dset == 2 or self.dset == 4: # Y or Zy
            self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.yp)
            self.dset = 1
            
        # Optimal config already setup, set up the rest of the pins
        elif self.dset == 3: # Zx
            self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.dset = 1  
            
        utime.sleep_us(5)
        self.xbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[2] = self.pread.read()
        self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2])/3)

        self.pos[0] = round(self.xCal*(self.pos[0]-self.cCal[0]),2)
        
        return(int(self.pos[0]))
        
    def yreader(self):
        '''@brief  read the y position from the resistive touch panel
           @details 
           @return self.pos the y value of the touch panel location in[mm]
        '''
        
        # Reset Pins for y-read in Optimal Configuration
        if self.dset == 1 or self.dset == 3: # X or Zx
        # If the previous set was X or Zx, Configure all:
                
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.xm)
            self.dset = 2
            
        elif self.dset == 4: # Zy
        # If the previous set was Zy, Configure s.t. ADC & High already set:
                            
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 2

        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.ybuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[2] = self.pread.read()
        self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2])/3)
        
        self.pos[1] = round(self.yCal*(self.pos[1]-self.cCal[1]),2)
        
        return(int(self.pos[1]))
    
    
    def zreader(self):
        '''@brief  read the z position from the resistive touch panel
           @details 
           @return slef.pos the Z value o fthe touch location in [mm]
           @return A boolean flag on whether the plate is being touched or not
        '''
        
        # Reset Pins for z-read in Optimal Configuration
        if self.dset == 2:
        # If the previous set was Y, Configure s.t. ADC & High already set:
                
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 4
            
        elif self.dset == 1:
        # If the previous set was X, Configure s.t. ADC & Low already set:
                            
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 3

        
        # Read Sensor Data & filter
        
        utime.sleep_us(5)
        self.zbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[2] = self.pread.read()
        
        self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2])/3)
        
        # If ADC reads less than high, or greater than low, touch detected:
        if 16 < self.pos[2] < 3500:
            self.tch = True
        else:
            self.tch = False
        
    def read(self):
        '''@brief   Read all three locations (x,y,and z)
           @details 
           @return The X,Y, and Z values of the touch loaction in [mm]
        '''
        
        if self.tch == False:
            self.zreader()
            if self.tch == True:
                self.xreader()
                self.yreader()
        else:
            self.xreader()
            if self.dset == 1:
                self.zreader()
                self.yreader()
            else:
                self.yreader()
                self.zreader()
                self.xreader()
                
        return self.pos
        
    
if __name__ == '__main__':
    while True:
        try:
            test = input('\n'*20 + 'What direction would you like to test?\n     - For X direction, enter "x"\n     -For Y direction, enter "y"\n     -For contact test, enter "z"\n     -For full-system test, enter "Full"\n\n                    >>>     ')
            if test != 'x' and test != 'y' and test != 'z' and test != 'Full':
                test = 'Full'

            
            tch = Touch_Driver('PA0','PA6','PA1','PA7')
            zflag = False
            
            if test == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        xpos = tch.xreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: X = ' + str(xpos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        ypos = tch.yreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: Y = ' + str(ypos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'z':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        [zpos, zflag] = tch.zreader()
                        if zflag == True:
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n' * 20 + 'You are touching the screen' + '\n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n' * 20 + 'You are not touching the screen' + '\n'*7)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'Full':
                while True:
                    try:
                        
                        if tch.tch == False:
                            t0 = utime.ticks_us()
                            tch.zreader()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                        if tch.tch == True:
                            t0 = utime.ticks_us()
                            position = tch.read()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n'*20)
                            
                            # dsp.disp(tch)
                            
                            print('\n'*2 +' You are touching at: X = ' + str(position[0]) +'     Y = ' + str(position[1]) + '     Z = ' + str(position[2]) + ' \n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n'*20)
                            # dsp.disp(tch)
                            print('\n' * 2 + 'You are not touching the screen' + '\n'*2)
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
        except KeyboardInterrupt:
            print('\n'*50 + 'Sucessfuly Exited Test Program \n\n')
            break