''' @file    hmwrk2.py 
    @brief   Documentation for use of hmwrk2.py. A State Space Anylysis
    @details This code hold the PDF document of the state space analysis of 
             the ME405 Term Project. 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    April. 29, 2020
    @INCLUDE ME405_hmw2.pdf
'''
