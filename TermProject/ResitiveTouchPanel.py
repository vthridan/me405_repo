'''@file touch_panel.py
   @brief This script holds the code to run the resitive touch panel
   @details 
   @author Nicholas Holman
   @author Vikram Thridandum
'''

import pyb
import array

## Pin obejct for pin 1 ym on touch panel
PA0 = pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.OUT_PP)
## Pin obejct for pin 2 xm on touch panel
PA1 = pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.OUT_PP)
## Pin obejct for pin 3 yp on touch panel
PA6 = pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.OUT_PP)
## Pin obejct for pin 4 xp on touch panel
PA7 = pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.OUT_PP)
## Create the ADC for the necessary pins
adc0 = pyb.ADC(pyb.Pin.board.X1) # ADC for ym
adc1 = pyb.ADC(pyb.Pin.board.X2) # ADC for xm
adc2 = pyb.ADC(pyb.Pin.board.X3) # ADC for yp
adc3 = pyb.ADC(pyb.Pin.board.X4) # ADC for xp

tim = pyb.Timer(8, freq=10)
## Buffer Array for ADC
buffy = array.array('H', (0 for index in range(1000)))

try:
    while True:
        for data in buffy 
            val = adc0.read()
            print(val)
            buffy.append(val)

except KeyboardInterrupt:
    print('Code stopped. Resistive touch panel disabled')
    break
