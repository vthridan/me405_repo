def fibSequence(numItems):
    
    # Define zeroth seed value (f0)
    f0 = 0
    yield(f0)
    
    # Define first seed value (f1)
    f1 = 1
    yield(f1)
    
    # Define and yield all further Fibonacci numbers
    counter = 2
    while(counter < numItems):
        counter += 1
        
        # Calculate new fib number
        f2 = f0 + f1
        
        # Preparing for the next iteration of the loop
        f0 = f1
        f1 = f2
        
        # yield the new fib number
        yield(f2)
    