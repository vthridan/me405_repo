var namespaces_dup =
[
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "fibGen", null, [
      [ "fibSequence", "fibGen_8py.html#af7f03b9a348d800c267d01f8c6830f77", null ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "main", null, [
      [ "moto", "main_8py.html#a385bbf44e78f0a990eafaf8d62b3b606", null ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "PythonDay_1", null, [
      [ "MyAdd", "PythonDay__1_8py.html#aba8e0af03ccf99829e4a032ff4b87ada", null ],
      [ "x", "PythonDay__1_8py.html#a6a43a2efc3765af2f0a72635160a2c46", null ],
      [ "y", "PythonDay__1_8py.html#ad99dc9b90d5dabd5f2a6939f50409142", null ],
      [ "z", "PythonDay__1_8py.html#a8f9a59e74c405e7c0f82f6e0d253d870", null ]
    ] ]
];