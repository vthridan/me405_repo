'''@file        mainpage.py
   @brief       Brief doc for mainpage.py
   @details     Detailed doc for mainpage.py
   
   @mainpage
   
   @section sec_intro Introduction
   Hello there! Welcome to my ME405 Portfolio. This is for Spring Quarter 2021.\n
   My lab partner is Nicholas Holman, here's his repo: https://bitbucket.org/nico9999/me405_labs/src/master/ \n
   Please see individual modules for details.
   
   Included modules are:
   * \ref page_lab0 \n
   * \ref page_lab1 \n
   * \ref page_lab2 \n
   * \ref page_hw2 \n
   * \ref page_lab3 \n
   * \ref page_hw4 \n
   * \ref page_lab4 \n
   * \ref page_TP \n
   
   This course extensively used a Nucleo L476RG board, shown below:
   \image html nucleo_pic.png
   
   @page page_lab0 Lab0x00: Preliminary Hardware & Toolchain Installation
   @section page_lab0_desc Description
   Here is a document showing I have the necessary hardware:
   \image html PrelimHardAcq_Page_1.png width=100%
   \image html PrelimHardAcq_Page_2.png width=100%
   
   @section page_lab0_doc Documentation
   Here is video proof the toolchain is working: https://youtu.be/r-gMPSvo6go 
   
      
   
   @page page_lab1 Lab0x01: Vendotron FSM & UI Development
   @section page_lab1_desc Description
   This lab was to develop a program that embodies a finite state machine for a vending machine called Vendotron.
   The code must run cooperatively and must include the following features:
   1. Display a startup message for the user
   2. Allow the user to select their preferred beverage at any time.
   3. Accept coins or bills.
   4. Accept the eject ('E') from the user at any time, at which point the initialization message
   should be displayed, and the balance returns to zero. \n\n
   
   \image html vendotron.png
   
   Vendotron sells 4 different drinks: Cuke ($1.00), Popsi ($1.20), Spryte ($0.85), and Dr. Pupper ($1.10). 
   The user can insert funds by pressing keys 0 through 7 on the keypad, each number corresponding to a different denomination of money:
   0 = penny ($0.01)
   1 = nickel ($0.05)
   2 = dime ($0.10)
   3 = quarter ($0.25)
   4 = $1 bill
   5 = $5 bill
   6 = $10 bill
   7 = $20 bill
   
   Vendotron always shows the current amount of funds in the machine after adding money. 
   Then the user can select their beverage of choice by typing keys as follows:
   C = Cuke
   P = Popsi
   S = Spryte
   D = Dr. Pupper
      
   @section page_lab1_fsm Finite State Machine
   Here is my FSM for this lab:
   \image html lab1_FSM.png
   This FSM accounts for the following behaviours:
   * On startup, Vendotron displays an initialization message on the LCD screen. \n\n
   
   * At any time a coin may be inserted. When a coin is inserted, the balance displayed on the
   screen must reflect so. \n\n
   
   * At any time a drink may be selected. If the current balance is sufficient, Vendotron vends
   the desired beverage through the flap at the bottom of the machine and then computes the
   correct resulting balance. If the current balance is insufficient to make the purchase then
   the machine displays an ”Insufficient Funds” message and then displays the price for the
   selected item. \n\n
   
   * At any time the Eject button may be pressed in which case the machine returns the full
   balance through the coin return.\n\n
   
   * The vending machine must prompt the user to select a second beverage if there is remaining
   balance.\n\n
   
   * If the vending machine is idle for a certain amount of time, it should occasionally show a
   scrolling message on the LCD display such as “Try Cuke today!”. \n\n
   
   @section page_lab1_src Source Code
   * <b>Source</b>: https://bitbucket.org/vthridan/me405_repo/src/master/Lab0x01/Lab0x01.py
   
   @section page_lab1_doc Documentation
   Here is a link to the documentation for the Vendotron FSM lab: \ref Lab0x01.py
   
   
   
   @page page_lab2 Lab0x02: Think Fast! (ISRs & Timing)
   @section page_lab2_desc Description
   DESCRIPTION HERE
   @section page_lab2_src Source Code 
   Here is our source code for Lab0x02: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%202/Lab2_Try2.py
   Here is a video we took showing the green LED and PuTTY: https://youtu.be/xtU9w3PJ-QI 
   @section page_lab2_doc Documentation
   Here is a link to our file's documentation: \ref Lab2_Try2.py
   
   
   
   @page page_hw2 HW0x02 Term Project System Modeling
   @section page_hw2_desc Description
   In this assignment we developed a simplified model of a pivoting platform
   so that we can design a controller to balance a ball atop the platform shown below.
   \image html platform.png
   Here are hand calcs for our model:  
   \image html ME405_hmw2_Page_1.png width=100%
   \image html ME405_hmw2_Page_2.png width=100%
   \image html ME405_hmw2_Page_3.png width=100%
   \image html ME405_hmw2_Page_4.png width=100%
   
    
   
   @page page_lab3 Lab0x03 Pushing the Right Buttons (ADC & ISRs)
   @section page_lab3_desc Description
   This lab uses a UI interface and a background running code to find the time constant for the blue button on the Nucleo.
   Once a command is sent from the UI to the Nucleo, ADC voltage data was collected from pinA5, which was wired to the pushbutton pin PC13. 
   The data was recored and exported to excel wehre we plotted the data.
   
   @section page_lab3_src Source Code
   * <b>UI Front End Source</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%203/UI_lab3_tests.py
   \n\n
   * <b>Nucleo main.py Source</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%203/main.py 
   \n\n
   * <b>CSV file written by our program</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%203/ADC%20Voltage%20Output20210507-142308.csv
   \n\n
   * <b>Plot from the CSV data</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%203/ME405_Lab3_VoltageGraph.PNG
   * <b>UI Documentation</b>: \ref UI_lab3_tests.py
   * <b>Nucleo main.py Documentation</b>: \ref Lab3main.py
   \n\n
   Here is the plot for our best step response test:  
   \image html ME405_Lab3_VoltageGraph.jpg
   The time constant associated with this step response is 0.498 ms.
   \n
   The RC time constant calculated from the component values is 0.500 ms.
   
   
   
   @page page_hw4 HW0x04 Simulation or Reality? (Linearization and Simulation of System Model)
   @section page_hw4_desc Description
   In this assignment we developed a simulation and tested a controller for the 
   simplified model we did in \ref page_hw2. We chose to use MATLAB and Simulink.
   
   Here are the numerical parameters we used in our simulation:
   \image html hw4params.png
   
   @section page_hw4_OL Open-Loop Simulink Model:
   Here is our open-loop simulink block diagram:
   \image html ME405_open_sim_files.PNG
   
   We ran this open-loop simulation for following cases and generated plots 
   showing the dynamic response for x, theta, xdot, and thetadot, each plotted as functions of time.
   
   * <b>Case A</b>: The ball is initially at rest on a level platform directly above the center of gravity of theplatform and there is no torque input from the motor. Run this simulation for 1 [s].
   * <b>Case B</b>: The  ball  is  initially  at  rest  on  a  level  platform  offset  horizontally  from  the  center  ofgravity of the platform by5 [cm]and there is no torque input from the motor. Run thissimulation for 0.4 [s].
   \n\n
   Here are our generated plots for open-loop:
   \image html ME405_open1_x.png 
   The ball is initially at rest here, and as long as there are no external forces, the ball shouldn't move.
   
   \image html ME405_open1_xdot.png 
   The ball is initially at rest here, and as long as there are no external forces, the ball shouldn't move.
   
   \image html ME405_open1_theta.png 
   The ball is initially at rest here, and as long as there are no external forces, the ball shouldn't move.
   
   \image html ME405_open1_thetadot.png
   The ball is initially at rest here, and as long as there are no external forces, the ball shouldn't move.
   
   \image html ME405_open2_x.png
   This is the second case, where the ball is horizontally offset by 5 cm, and there's no torque input from the motor.
   The system is unstable due to the positive pole, therefore the system diverges from zero.
   
   \image html ME405_open2_xdot.png
   There is no torque input from the motor, and therefore the sytem diverges.
   
   \image html ME405_open2_theta.png
   There's no torque input from the motor, and since the system is unstable, it will diverge.
   
   \image html ME405_open2_thetadot.png
   Similarly to above, the system is unstable, and with no torque input, the system diverges.
   
   @section page_hw4_CL Closed-Loop Simulink Model:
   \image html ME405_closed_sim_file.PNG
   
   Here we tested our simulation in closed-loop by implementing a regulator 
   using full state feedback (u = −Kx). In matrix notation, this equation is as shown:
   \image html hw4CLfeed.png
   
   Instead of designing our own controller, we used these given gains to test
   our system performance in closed-loop:
   \image html hw4CLgains.png   
   \n\n
   Here are our generated plots for closed-loop:
   \image html ME405_closed_x.png
   
   The closed loop feedback system helped the system converge to a solution, but we're unsure why it went to -5 meters instead of zero.
   
   \image html ME405_closed_xdot.png
   
   The closed loop feedback system we implemented helped these parameters converge to a solution.
   
   \image html ME405_closed_theta.png
   
   The closed loop feedback system we implemented helped these parameters converge to a solution.
   
   \image html ME405_closed_thetadot.png
   
   The closed loop feedback system we implemented helped these parameters converge to a solution.
   
   @section page_hw4_src Source Code
   Our MATLAB file is located here: https://bitbucket.org/vthridan/me405_repo/src/master/HW0x04/ME405_Hmw4.m
   
   
   
   @page page_lab4 Lab0x04: Hot or Not? (Interfacing with Sensors using I2C)
   @section page_lab4_desc Description
   In this lab, The nucleo L476RG is used to measure various temperatures at a given interval (60 seconds)
   
   * <b>Main Source</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%204/Lab0x04.py
      * This takes measurements every 60 seconds, and writes it to a CSV file.
   \n\n    
   * <b>Custom Python Module mcp9808.py</b>: https://bitbucket.org/nico9999/me405_labs/src/master/Lab%204/mcp9808.py
      * This allows us to communicate with the MCP9808's temperature sensor using its I2C interface.
   \n\n    
   * <b>Generated .CSV file</b>: https://bitbucket.org/vthridan/me405_repo/src/master/Lab0x04/Temperatures_Nucleo.csv
      
   * Here is my temperature vs. time plot:
      
   \image html ME405_Lab4_TempTime.png width=100%
   
   * This data was taken overnight (11:50pm - 2:10pm) in Carlsbad, CA from May 12 - 13, 2021.
   
   @section page_lab4_doc Documentation
   Here is the documentation link for our main file: \ref Lab0x04.py \n
   Here is the documentation link for the MCP9808 class: \ref mcp9808.py
   
   
   
   @page page_TP Term Project: Balancing Platform
   @section page_TP_desc Description
   This project aims to balance a ball atop the platform shown in \ref page_hw2.
   \image html final_assy.png
   
   @section page_TP_hw Hardware
   This shows all the hardware we used. This includes:
   * DCX22S stepper motors
   * Encoder
   * Nucleo Board
   * bno055 IMU sensor
   * Touch Panel
   * The connection pieces created by Charlie Refvem
   
   @section page_TP_base Base Mount
   Here is the base mount for the balancing platform, which has a Nucleo L476RG mounted on a custom PCB 
   created by Charlie Refvem (CC BY-NC-SA 4.0)
   \image html base_mount.png
   
   @section page_TP_motors DC Motors
   We used two of these DCX22S stepper motors, controlled by \ref motor_driver.py .
   \image html motor.png
   These are connected via belt to another gear attached to an output shaft, which also has an encoder.
   The motors could be controlled by either the Encoder or an IMU.
   For us we would get an error base (!=) a0. After hours of trying to figure out the error, we opted to collect data with the Encoder.
    
   @section page_TP_sns Sensors
   * <b>Output Shaft Encoder</b>
   \image html output_encoder.png
   The encoders are used to determine the tilt angles of the entire platform. 
   They are driven by the encoder driver (\ref Encoder.py). The encoders are 
   attached to the custom PCB and give real-time readings to the Nucleo. 
   The x encoder relates the y-axis tilt, and the y encoder relates to the x-axis tilt.
   We ended up using these encoders instead of the BNO055 IMU because we kept getting an error. 
   \n\n
   * <b>BNO055 IMU</b>
   \image html bno055.png
   We didn't end up using this because we kept getting error "base (!=) a0"
   \n\n
   * <b>Touch Panel</b>
   \image html touchpanel.png
   This acts as a voltage driver with varying voltages depending on where the
   ball is located in the two-dimensional x and y planes. This communicates 
   with the Nucleo via the FPC breakout board, which is connected to the X6 pin
   on the PCB.
    
   @section page_TP_sw Software
   This section describes all the drivers we wrote for this lab assignment. 
   All of these files can be found under the classes and files section to the left.
   
   @section page_TP_tskshare Task Queues and Sharing
   This section explains the shares and queues we used to allow for cooperative task execution. 
   Here are the variables that are being shared between tasks:
   \image html ME405_Shared_var.png
   The two main files used for multitaksing were: \ref cotask.py and \ref task_share.py.
   
   * \ref cotask.py : We got this from Charlie. This file contains classes to run cooperatively scheduled tasks in a
   multitasking system.  This file allows the user to create task objects with assigned priority and period.
   For our case we had to ensure the period was long enough for each task to run and have spare time to allow for other tasks to execute. 
   The priority scheduler performs which ever task has the highest priority, but when each task has a different period, 
   other tasks can be called while the higher priority task waits to execute again. 
   The tasks happen so fast that it seems they are occuring simultaneously. We divided our code into 5 tasks:
   \n \n
   1. X-Axis controller
   2. Y-Axis Controller
   3. Touch Panel Data Collection
   4. Encoder Data Collection
   5. IMU Data Collection
   \n \n
   Either the Encoder or IMU were used to collect data. These two cannot operate at the same time, so only 4 of these tasks were operating at once.
   
   * \ref task_share.py : We also got this from Charlie.
   This file contains classes which allow tasks to share data without the risk
   of data corruption by interrupts. This allows for the creating of Queue and Share objects so that data and booleans could be shared between tasks. 
   For our sake, each state space variable form the state space models were assigned a queue. 
   The diagram above lays out how the shares and queues communicated between tasks.
   
   * <b>X-Axis Controller</b>: This task takes data from the queues filled by the other tasks and performs unit conversion 
   and a state space model to determine the necessary torque for the system to correct itself. 
   The torque is then converted to a duty cycle and fed to the X-axis motor. 
   We assigned this motor the quickest period with the third highest priority, right behind the Y-axis controller.
   \n\n
   * <b>Y-Axis Controller</b>: This task takes data from the queues similar to the X-axis controller. 
   It performs the same operations and conversions for the Y-axis. 
   This task was given the second highest priority behind the touch panel (\ref touch_panel.py) , with the second fastest period.
   
   * <b>Touch Panel Data Collection</b>: This task calls on the \ref Touch_Driver.py to collect data from the resistive touch panel. 
   It then processes the data using time stamps from utime to find the position and velocity of the ball. 
   Then it stores those values in a queue to be accessed by the X and Y axis controller tasks. 
   This task was given the highest priority with the slowest period.
   
   * <b>Encoder Data Collection</b>: This task calls on the \ref Encoder.py driver to collect angular position of the output shaft from the motor encoder. 
   This data is then processed to find the angular position and acceleration of the platform using geometry of the system and time stamps. 
   Then the data stored in queue to be used by the X and Y axis controllers.
   
   * <b>IMU Data Collection</b>: This task calls on the \ref bno055.py driver to collect gyroscopic data from the IMU. 
   This sensor is attatched to the top of the board for more accurate data collection. 
   We ended up not using this driver or task due to an error (base (!=) a0). This task was run at the same period and priority as the Encoder.
      
   @section page_TP_driv Drivers
   * \ref Encoder.py : This handles the class that interfaces between the motor encoders attached to the output shafts and the Nucleo board. 
   Please see classes section for a more detailed description of each driver/class.
      
   * \ref bno055.py : This handles the outputs and calibration of the BNO055 IMU. 
   This code was found via an open source Github and was written by Radomir Dopieralski for Adafruit Industries.
   Copyright 2017 Radomir Dopieralski for Adafruit Industries, The MIT License 
   
   * \ref bno055_base.py : This calls the BNO055 IMU on the base board for initialization setup.
   Copyright 2017 Radomir Dopieralski for Adafruit Industries, The MIT License
      
   * \ref Touch_Driver.py : This handles the class to control the resistive touch panel. 
   It reads three dimensions on the panel. It provides two state variables, x and y.
   
   * \ref motor_driver.py : This handles the class to control the two DCX22S motors by giving the
   functionality to enable and disable the motors, and a PWM duty cycle to drive the motors
   at specified velocities.
   
   @section page_TP_lsn Lessons Learned
   In the end we were unable to successfully balance the ball on the platform. 
   The attached youtube video shows the attempts and gives context as to why it failed. 
   https://www.youtube.com/watch?v=xlWCQHvxXXY
   The video shows how the touch panel began to malfunction at the end of the project. 
   At first the touch panel would correctly read when there was contact and no contact. 
   However, as the device sat in Nicholas' room, the panel began to develop problems. 
   We had to consistently bring the upper ADC voltage limit to what it was reading when contact was initiated. 
   Eventually the touch panel began to output random ADC values whether or not contact existed. 
   This led to a frustrating 18-hour debugging session that resulted in us figuring out that the wiring connecting the panel to the board was the problem. 
   Whenever the wires touched anything, the touch panel would output that contact was initiated at x = -65 and y = 20. 
   This caused the motors to constantly spin in one direction. Unfortunately that direction happened to cause more contact to the wires which made the problem worse.
   We tried electrical tape and a few other solutions to prevent the wires from sending bad data, but nothing helped. 
   I also had a similar problem along with other complications, such that we were unable to get a successful attempt on video.
   
   @section page_TP_doc Documentation
   Below is the link to the YouTube video once again for documentation purposes: 
   https://youtu.be/xlWCQHvxXXY
   
   This project would not have been possible without Nicholas Holman's help.
   
   @authors Vikram Thridandam, Nicholas Holman
   
   @date June 11, 2021
'''