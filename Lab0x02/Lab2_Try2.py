''' @file    Lab2_Try2.py 
    @brief   Documentation for use of Lab 0x02.py. Reaction Test. Think Fast!
    @details This code randomly lights an LED on a STM32L476 Nucleo for one 
             second. The user then must press the blue push button before the 
             LED truns off. When the user starts the program the LED will 
             light up randomly every 2 to 3 seconds. The user will then have 
             one second to press the blue button on the nucleo. The code will
             take the time between the LED lighting up and the button press 
             and store that value. Once the user quits the program the average
             reaction time is recorded. 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    March 2, 2020
'''

import utime
import random
import pyb
import micropython
from pyb import Timer, Pin, delay
micropython.alloc_emergency_exception_buf(100)
pyb.enable_irq   #Allows for interrupt

#Declare Global Variables
global lastCompare
global pin_var
global t_pushed
global app_var
global pinA5
global pinB3
global delay_var

# Initialize switch, timers, and pins
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)    #Initialize LED pin
pinB3 = pyb.Pin(pyb.Pin.cpu.B3, mode=pyb.Pin.IN)   #Initialize Pushbutton pin
tim2 = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF) #Initialize Timer w/ prescalar
# tim5 = pyb.Timer(5, prescaler = 79, period = 0x7FFFFFFF) #Initialize Timer w/ prescalar

# set variable for the main code
app_var = 0
pin_var = 0
t_pushed = [0]
# start_time = utime.ticks_us()

def OC_CB (Tim):
    ''' @brief   An output compare intrrupt that runs on a toggle
        @details The output compare triggers after a certain amount ticks have 
                 been recorded on rissing and falling edges. The lastCompare
                 uses the previous value of ticks to find the new value to 
                 trigger the interupt agian
    '''
    global lastCompare
    global pinA5
    global delay_var
    print('Timer Value is: ')
    # print(tim2ch1.counter())
    lastCompare = tim2ch1.compare()
    lastCompare += 2000000                #adds one second to the last capture time
    # lastCompare &= 0x7FFFFFFF
    tim2ch1.compare(lastCompare)
    pinA5.low()
    # utime.sleep(delay_var)            #Pause for duration of delay
    # if pin_var == 1:
    #     pin_var = 0
    #     app_var = 1
    #     print('Appending time to list.')
    #     # t_pushed.append(tim2.counter)
    # else:
    #     pass
    # print('lastCompare is: ')
    # print(lastCompare)
    print('OC called')
    
    
def IC_CB(Tim):
    '''@brief   An input compare interrupt that runs when the PB3 is detected
       @details The input compare triggers when the blue push button is 
                released on the Nucleo. It then captures the time from when 
                the LED lite up and the button was released. 
    '''
    global last_cb_time
    global t_pushed
    global app_var
    last_cb_time = tim2.channel(2).capture()
    # t_pushed.append(last_cb_time)
    app_var = 1
    print('IC Called')
    # return app_var
    

# tim2ch1 = tim2.channel(1, pin=Pin.cpu.A5, mode=Timer.OC_TOGGLE, polarity=Timer.HIGH, callback=None)
# tim2ch2 = tim2.channel(2, pin=Pin.cpu.B3, mode=Timer.IC, callback=IC_CB, polarity=Timer.FALLING)

# set variables for the interrupts 
last_cb_time = 0
lastCompare = 0

# set up code for interrupts 
extint = pyb.ExtInt(pinB3, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback=OC_CB) 


try: 
    # tim2ch1 = TIM2.channel(1, pin+Pin.cpu.A5, mode=Timer.OC_TOGGLE, callback=OC_CB, polarity=Timer.HIGH)
    tim2ch1 = tim2.channel(1, pin=Pin.cpu.A5, mode=Timer.OC_TOGGLE, polarity=Timer.HIGH, callback=OC_CB)
    tim2ch2 = tim2.channel(2, pin=Pin.cpu.B3, mode=Timer.IC, callback=IC_CB, polarity=Timer.FALLING)
    while True:
        global pin_var
        global app_var

        if app_var == 1:
            # takes the gloabl variable from the IC and adds the time to a list
            print('counter val appended is: ' + str(last_cb_time))  # print for debugging
            t_pushed.append(last_cb_time)    # add the new time to the list
            app_var = 0                      #clear variable from IC to note the value has been recorded
        else: 
            pass
        print('in manin loop for debugg')
        variance = random.randint(1,1000) #Generate random integer
        delay_var = 2+variance/1000       #Apply random integer to LED delay
        tim2.counter(0)                   #Prevent triggering of delay before LED is illuminated
        utime.sleep(delay_var)            #Pause for duration of delay
        pinA5.high()                      #Turn on LED
        pin_var = 1                       #A pin variable for debugging
        print('LED Should turn on here')
        tim2.counter(0)                   #'Start' Timer
        utime.sleep(1)                    #Leave LED on for one second regardless of pushbutton
        print(tim2.counter())
        delay(200)
    
except KeyboardInterrupt:
    tim2.deinit()
    Pin(Pin.cpu.A5, mode=Pin.OUT_PP)
    print(t_pushed)
    if len(t_pushed)>1:
        aresponse = sum(t_pushed)/(len(t_pushed)-1)     # get average time subtract one to account for the 0 in the first slot of the list
    else:
        aresponse = t_pushed[0]
    print('Your average response time is: ' + str(aresponse) + ' microseconds.')
    #quit()
    
    
    
    
    
    