%%% ME405 Simulation or Reality
% Simulation to test a controller for the term project
% <insert name of simulink file here as well>

close all
clear all
%% Givens
% Based on the givens on the assignment sheet we found the Linearized SS
% model  A and B for the equation x_dot = Ax+Bu
A = [0,        0,   1, 0
     0,        0,   0, 1
     -5.217, 4.011, 0, 0.1773
     112.9, 64.83,  0, -3.836];
B = [0
     0
     32.5
     -703.2];
C = [1, 0, 0, 0
     0, 1, 0, 0
     0, 0, 1, 0
     0, 0, 0, 1];
D = [0
     0
     0
     0];

K = [-0.3, -0.2, -0.05, -0.02];
 
%% Open Loop analysis
% The ball is initially at rest on a level platform directly above the
% center of gravity of the platform and there is no torque input from the
% motor (simulated for 1 second)
% Case two the ball starts 5cm away from the center
%ICs case one = [0, 0, 0, 0]
%ICs case two = [0.5, 0, 0, 0]
sim('ME405_Hmw4_sim'); %This file sims case1 and case2
tout = ans.tout;
X1 = ans.X1;
X2 = ans.X2;

figure(1)
plot(tout,X1(:,1))
xlabel('Time (s)')
ylabel('x (m)')
legend('x')

figure(2)
plot(tout,X1(:,2))
xlabel('Time [s]')
ylabel('theta [rad]')
legend('theta')

figure(3)
plot(tout,X1(:,3))
xlabel('Time [s]')
ylabel('xdot [m/s]')
legend('xdot')

figure(4)
plot(tout,X1(:,4))
xlabel('Time [s]')
ylabel('thetadot [rad/sec]')
legend('thetadot')

figure(5)
plot(tout,X2(:,1))
xlabel('Time [s]')
ylabel('x (m)')
legend('x')

figure(6)
plot(tout,X2(:,2))
xlabel('Time [s]')
ylabel('theta [rad]')
legend('theta')

figure(7)
plot(tout,X2(:,3))
xlabel('Time [s]')
ylabel('xdot [m/s]')
legend('xdot')

figure(8)
plot(tout,X2(:,4))
xlabel('Time [s]')
ylabel('thetadot [rad/sec]')
legend('thetadot')


%% Closed Loop analysis
% Run the same tests as above but with a closed loop system. 
% Use the gains provided by Charlie
% K = [-0.3, -0.2, -0.05, -0.02];
% use ICs = [0.5, 0, 0, 0] again
sim('ME405_hw4_closed');
tout2 = ans.tout;
X3 = ans.X3;

figure(9)
plot(tout2,X3(:,1))
xlabel('Time [s]')
ylabel('x [m]')
legend('x')

figure(10)
plot(tout2,X3(:,2))
xlabel('Time [s]')
ylabel('theta [rad]')
legend('theta')

figure(11)
plot(tout2,X3(:,3))
xlabel('Time [s]')
ylabel('xdot [m/s]')
legend('xdot')

figure(12)
plot(tout2,X3(:,4))
xlabel('Time [s]')
ylabel('thetadot [rad/sec]')
legend('thetadot')



