''' @file Lab0x01.py 
    @brief      Uses a finite state machine to simulate a vending machine, Vendotron^TM^.
    @details    After a startup message, the user can select a desired beverage at any time. 
                The user can also input coins or bills at any time. The script will determine if the
                correct change has been provided, and return a drink, or an insufficient funds message. 
                At any point the user can eject their balance. 
    @author     Vikram Thridandam
'''

from time import sleep

def VendotronTask():
    """ Runs one iteration of the task
    """
    state = 0
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print("The state is",state)
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            # perform state 1 operations
            print("The state is",state)
            state = 2   # s1 -> s2
            
        elif state == 2:
            # perform state 2 operations
            print("The state is",state)
            state = 3   # s2 -> s3
            
        elif state == 3:
            # perform state 3 operations
            print("The state is",state)
            state = 1   # s3 -> s1
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)



if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')