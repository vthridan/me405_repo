''' @file    mcp9808.py 
    @brief   This class functions as the driver for the MCP9808 Temperature Sensor
    @details 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    May 21, 2021
'''

from pyb import I2C
import utime

class MCP9808:
    '''
    @brief Descirbe class here
    '''
    
    def __init__(self, address):
        '''@brief Initiate the class by setting the base and address
           @param address   the Device address connected to the MCP9808
        '''
        ## Create the I2C and initiate Master
        self.i2c = I2C(1, I2C.MASTER)
        self.addr = address
        
    def check(self):
        '''@breif  This function checks if the read value matched the manufacturer ID on the MCP9808
           @return A True or False boolean. True is the ID's match, False if the do not.
        '''
        ## Create a buffer to write the Manufacture ID to
        checkbuf = bytearray(2)
        ## Write the ID to the array
        self.i2c.mem_read(checkbuf, addr = self.addr, memaddr = 6)
        ## Verify published manufacturer ID matched the recieved ID
        if checkbuf[1] == 0x0054:
            return True
        else:
            return False

    def celsius(self):
        '''@brief  This function finds the temperature in Celsius measured by the sensor
           @return temp  The tempurature of the ambient air measured by the sensor
        '''
        print('reading temp C')
        ## Create the array to store the Temperature Data into
        buf = bytearray(2)
        #Write the data into the array
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        #Clear flags
        buf[0] = buf[0] & 0x1F
    
        # Record the data
        temp = buf[0]*16 + buf[1]/16   #Convert into a decimal
        return temp
    
    def fahrenheit(self):
        '''@brief This function finds the temperature in Fahrenheit measured by the sensor
           @return tempF The temperature in Farenheit measured by the sensor
        '''
        ## Create the array to store the Temperature data
        buf = bytearray(2)
        #Write the data into the array
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        #Clear flags
        buf[0] = buf[0] & 0x1F
        
        #Record the data
        temp = (buf[0]*16+buf[1]/16)
        tempF = temp*(9/5)+32
        return tempF
    
if __name__ == '__main__':
    #This function returns the temp in degrees celsius every second 
    mcp = MCP9808(24)   #24 is the memory address of the sensor
    mcp.check()
    while True:
        print(mcp.celsius())
        utime.sleep(1)
    
        
        