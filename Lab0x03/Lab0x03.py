''' @file    Lab0x03.py 
    @brief   
    @details 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    April. 29, 2020
'''

import array
import pyb
from pyb import Timer, Pin, delay, UART
pyb.repl_uart(None)
pyb.enable_irq

## UART, Pin, and timer initialization 
myuart = UART(2)
PA0 = pyb.Pin(pyb.Pin.board.PC0, mode=pyb.Pin.IN) #cpu.A0    # sets Pin C0 to the ADC to trigger with a button press
adc = pyb.ADC(PA0)
tim6 = pyb.Timer(6, freq = 200000)

## Variables to calculate ADC values
freq = 200000
ranger = 500
boolflag = None

## Set up arrays 
buffy = array.array('H', (0 for index in range(ranger)))
prebuffy = array.array('H', (0 for index in range(20)))
tim_data = array.array('H', (int(i*(ranger/freq)*1e3) for i in range(ranger)))

#Interrupt function
def pushed(Line):
    global boolflag
    global val
    if val == 'g':
        boolflag = 1
        
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback=pushed)


while True:
    if myuart.any() != 0:
        val = myuart.read().decode('ascii')
    elif boolflag == 1:
        boolflag = 0
        val = None
        break

while True:
    #Pre-read the ADC and insure that the signal will register
    adc.read_timed(prebuffy, tim6)
    if prebuffy[-1] >= 10:
        break
    
#Actaully read the ADC and send the data array through the serial port to the UI
adc.read_timed(buffy, tim6)
time = 0
period = 20

while True:
    for tim_data in buffy:
        myuart.write(str(buffy) + '  ' + str(tim_data) + '\r\n')
        time += period
        # ## add code to reject anything thats not a button press
        # while PA0.low():
        #     input('Press blue button on Nucleo to continue: ')
        #     if PA0.high():
        #         #if button pressed record data
        #         val = ADC.read()
        #         tim = pyb.Timer(2, freq=100)
        #         buf = array.array('H', (0 for index in range (200)))
        #         ADC.read_timed(buf, tim)
        #     else: 
        #         pass
            
        
    # except KeyboardInterrupt:
    #     quit()
