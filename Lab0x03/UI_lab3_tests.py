'''
@file UI_lab3_tests.py
@brief This file handles the User Interface side of Lab 3.
@details This file promts the user to press 'g' to begin data collection, and 
         rejects all other inputs. It then waits for the Nucleo to send the 
         data back via the serial port. Once the data is recieved, it plots 
         the data using pyplot from matplotlib, and creates a CSV file from 
         the data. 
@author Nicholas Holman
@author Vikram Thridandum 
@date May 7th, 2021
'''

## import necessary modules
from matplotlib import pyplot
import serial
import time
import csv

## Initialize serial port
ser = serial.Serial(port='COM5', baudrate = 115273,timeout=1)

print('''
      Welcome! This script allows you to prompt the collection
      of data related to the voltage response on the Nucleo L476
      to the User Pushbutton. To initiate data collection, press "g"
      ''')
      
## User key input
char = input('Press g to open data collection: ')

#Loop for preventing incorrect inputs
while True:
    if char != 'g':
        char = input('Invalid input. Please press g to open data collection: ')
    else:
        break
    
ser.write(str(char).encode('ascii'))
    

#Loop to wait for returned arrays from nucleo post collection
while True:
    ## wait for the Nucleo to send data back via the Serial port
    if ser.in_waiting != 0:

        ## Data variables being formatted into lists for plotting containing ADC data generated on the Nucleo
        data = ser.readline().decode('ascii').split('  ')
        out = [m.strip("array('H', [])\r\n").split(', ') for m in data]
        dat_out = [int(t)*(3.3/4095) for t in out[0]]
        time_stamp = [int(t) for t in out[1]]
        
        #Plotting Data
        pyplot.plot(time_stamp, dat_out)
        pyplot.xlabel('Time (ms)')
        pyplot.title('ADC Digitally Converted Voltage Response to User Pushbutton')
        pyplot.ylabel('ADC Voltage Output (V)')
        
        #Creating CSV File with the time stamp to seperate trials
        filename = 'ADC Voltage Output'+time.strftime('%Y%m%d-%H%M%S')+'.csv'
        with open(filename, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile)
            for n in range(len(time_stamp)):
                spamwriter.writerow([time_stamp[n], dat_out[n]])
            

        break