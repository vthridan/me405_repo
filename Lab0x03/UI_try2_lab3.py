''' @file    UI_front_lab3.py 
    @brief   This file hold shte code for the PC UI for initiation of data 
             collection and graphing
    @details Once the use presses "g" to initiate data collection it starts 
             the collection of data in Lab0x03 on the Nucleo. The file then
             waits for the data to be sent back via the serial port to graph
             the data using the matplotlib function
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    April. 29, 2020
'''


import serial
import keyboard
import array
from matplotlib import pyplot
import csv

## Initialize the serial port
ser = serial.Serial(port='COM5',baudrate=115200,timeout=1)

volts = array.array('f',100*[0])
time = array.array('f',100*[0])

## Wait for user key input
val = input('Press "g" to open data collection: ')


# def sendChar():
with serial.Serial('COM5',baudrate=115200,timeout=1) as ser:
    ser.write(str('g').encode())
    if ser.in_waiting != 0:
        for line in ser:
            line = ser.readline().decode()
            print('Line = ',line)
            print('Stripped Line = ', line.strip())
            [t,y] = line.strip().split('  ')
            print('y = ',y)
            print('t = ',t)
            time.append(float(t))
            volts.append(float(y))
            print('')

## Plot the data
pyplot.plot(time, volts)
pyplot.xlabel('Time (ms)')
pyplot.ylabel('ADC Output Voltage (v)')
pyplot.title('ADC DC Voltage Response with a User Button')

## Making the CSV file for plots
filename = 'ADC Output Voltage'+'.csv'
with open(filename,'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile)
    for n in range(len(time)):
        spamwriter.writerow([time[n], volts[n]])
# while True:
#     if ser.in_waiting != 0:
#         ## format the data into lists for plotting the ADC data
#         data = ser.readline().decode('ascii').split('  ')
#         out = [m.strip("array('H', [])\n").split(', ') for m in data]
#         voltage = [int(t)*(3.3/4095) for t in out[0]]
#         tim = [int(t) for t in out[1]]
        
#         ## Plot the data
#         pyplot.plot(tim, voltage)
#         pyplot.xlabel('Time (ms)')
#         pyplot.ylabel('ADC Output Voltage (v)')
#         pyplot.title('ADC DC Voltage Response with a User Button')
        
#         ## Making the CSV file for plots
#         filename = 'ADC Output Voltage'+'.csv'
#         with open(filename,'w', newline='') as csvfile:
#             spamwriter = csv.writer(csvfile)
#             for n in range(len(tim)):
#                 spamwriter.writerow([tim[n], voltage[n]])
                
    

# def sendChar():
#     ''' @brief  function that sends a character to the serial port on the Nucleo
#         @return myval   returns an array of data from the serial port
#     '''
#     # inv = input('Give me a character: ')    # this line had intial code to learn how serial works
#     inv = last_key
#     ser.write(str(inv).encode())
#     myval = ser.readline().decode()
#     return myval

# def kb_cb(key):
#     ''' @brief Callback function which is called when a key has been pressed.
#     '''
#     global last_key
#     last_key = key.name

# for n in range(1):
#     print(sendChar())
    
    
# try: 
#     while True:
#         if state == 0:
#             print('Press G to begin data collection: ')
#             state =1
#         elif state ==1:
#             data = sendChar()
            
            
            
#         elif state == 6:
#             ser.close()
            
#         # might need to put next two lines in state 0 will determine this through testing    
#         lastkey = ''
#         keyboard.on_release_key("G", callback=sendChar())  # input for data collection to begin
